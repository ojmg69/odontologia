import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "vue-swatches/dist/vue-swatches.css"
import VueKonva from 'vue-konva'

//import { BootstrapVue } from 'bootstrap-vue'
//Vue.use(BootstrapVue)
// Import Bootstrap an BootstrapVue CSS files (order is important)
//import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'
//import { BIcon } from 'bootstrap-vue'
//Vue.component('b-icon', BIcon)
//import { IconsPlugin } from 'bootstrap-vue'
//Vue.use(IconsPlugin)
Vue.use(VueKonva)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
