import Vue from 'vue'
import Vuex from 'vuex'

//import datosjs from "../assets/datos.js";
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    numero:5,
    edad:20,
    leguajes:[]
  },
  mutations: {
    suma(state){
      state.numero++
    },
    resta(state,n){
      state.numero=state.numero-n
    },
    cargaLenguajes(state,leguajes){
      state.leguajes=leguajes;
  }
  },
  actions: {
    getLengujes: async function({commit}){
      const datos = await fetch('data/datos.js');
      const leguajes= await datos.json();
      commit('cargaLenguajes',leguajes);
  }
  },
  modules: {
  }
})
